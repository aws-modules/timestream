## Providers

| Name | Version |
|------|---------|
| <a name="provider_aws"></a> [aws](#provider\_aws) | n/a |

## Modules

No modules.

## Resources

| Name | Type |
|------|------|
| [aws_timestreamwrite_database.database](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/timestreamwrite_database) | resource |
| [aws_timestreamwrite_table.table](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/timestreamwrite_table) | resource |

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| <a name="input_application"></a> [application](#input\_application) | Name of the application | `string` | n/a | yes |
| <a name="input_database_map"></a> [database\_map](#input\_database\_map) | Enter database details,name,kms key | `map(any)` | `{}` | no |
| <a name="input_environment"></a> [environment](#input\_environment) | Name of the environment where you want to deploy the application/product | `string` | n/a | yes |
| <a name="input_kms_key"></a> [kms\_key](#input\_kms\_key) | KMS key to be used to encrypt the data stored in the database | `string` | `null` | no |
| <a name="input_product"></a> [product](#input\_product) | Name of the product | `string` | n/a | yes |
| <a name="input_retention_properties"></a> [retention\_properties](#input\_retention\_properties) | n/a | <pre>map(object({<br>    magnetic_store_retention_period_in_days = number<br>    memory_store_retention_period_in_hours  = number<br>  }))</pre> | `{}` | no |
| <a name="input_table_map"></a> [table\_map](#input\_table\_map) | n/a | `map(any)` | `{}` | no |
| <a name="input_use_case"></a> [use\_case](#input\_use\_case) | Name of the usecase | `string` | n/a | yes |

## Outputs

| Name | Description |
|------|-------------|
| <a name="output_database_arn"></a> [database\_arn](#output\_database\_arn) | n/a |
| <a name="output_database_id"></a> [database\_id](#output\_database\_id) | n/a |
| <a name="output_database_kms_key_id"></a> [database\_kms\_key\_id](#output\_database\_kms\_key\_id) | n/a |
| <a name="output_table_arn"></a> [table\_arn](#output\_table\_arn) | n/a |
