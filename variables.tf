variable "table_map" {
  type    = map(any)
  default = {}
}
variable "environment" {
  description = "Name of the environment where you want to deploy the application/product"
  type        = string
}
variable "product" {
  description = "Name of the product"
  type        = string
}
variable "application" {
  description = "Name of the application"
  type        = string
  default     = null
}
variable "use_case" {
  description = "Name of the usecase"
  type        = string
}
variable "kms_key_id" {
  type    = string
  default = null
}
variable "tags" {
  type    = map(any)
  default = {}
}
