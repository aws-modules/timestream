resource "aws_timestreamwrite_database" "this" {
  database_name = var.application != null ? "${var.environment}_${var.product}_${var.application}_${var.use_case}" : "${var.environment}_${var.product}_${var.use_case}"
  kms_key_id    = var.kms_key_id
  tags          = merge(var.tags, local.default_tags)
}
resource "aws_timestreamwrite_table" "this" {
  for_each      = var.table_map
  database_name = aws_timestreamwrite_database.this.database_name
  table_name    = each.key

  dynamic "retention_properties" {
    for_each = each.value.retention_properties
    content {
      magnetic_store_retention_period_in_days = retention_properties.value.magnetic_store_retention_period_in_days
      memory_store_retention_period_in_hours  = retention_properties.value.memory_store_retention_period_in_hours
    }
  }
  tags = merge(var.tags, local.default_tags)
}