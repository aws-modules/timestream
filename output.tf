output "database_id" {
  value = aws_timestreamwrite_database.this.id
}

output "database_arn" {
  value = aws_timestreamwrite_database.this.arn
}

output "table_id" {
  value = [
    for table_id in aws_timestreamwrite_table.this : table_id.id
  ]
}

output "table_arn" {
  value = [
    for table_arn in aws_timestreamwrite_table.this : table_arn.arn
  ]
}